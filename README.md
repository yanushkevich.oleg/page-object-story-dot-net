# Page Object pattern

**Estimated reading time**: 20 min

## Story Outline
Creating page objects involves refactoring the existing code snippet into classes that represent individual web pages and their elements. The purpose of using a page object model is to make the code more maintainable, readable, and reusable.  

## Story Organization
**Story Branch**: master
> `git checkout master`

Tags: #Selenium, #.NET, #C_Sharp, #Patterns