﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObject.Pages;

IWebDriver driver = new ChromeDriver();

try
{
    var indexPage = new IndexPage(driver);

    indexPage.Open().Search("Automation");
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
finally
{
    driver.Quit();
}