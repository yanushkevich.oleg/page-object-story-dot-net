---
focus: src/PageObject/Program.cs
---

### Conclusion

Page objects are a classic example of encapsulation - they hide the details of the UI structure and widgets from other components (the tests). It's a good design principle to look for situations like this as you develop - ask yourself "how can I hide some details from the rest of the software?". By confining logic that manipulates the UI to a single place you can modify it there without affecting other components in the system. A consequential benefit is that it makes the client (test) code easier to understand because the logic there is about the intention of the test and not cluttered by UI details.